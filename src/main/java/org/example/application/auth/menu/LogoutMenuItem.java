package org.example.application.auth.menu;

import lombok.RequiredArgsConstructor;
import org.example.application.auth.AuthService;
import org.example.application.auth.User;
import org.example.application.auth.UserView;
import org.example.application.infrastructure.menu.MenuItem;

@RequiredArgsConstructor
public class LogoutMenuItem implements MenuItem {

    private final AuthService authService;

    @Override
    public String getName() {
        return "Logout";
    }

    @Override
    public void run() {
        authService.exit();
    }

    @Override
    public boolean isVisible() {
        return authService.isAuth();
    }
}
