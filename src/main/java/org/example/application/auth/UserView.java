package org.example.application.auth;

import lombok.RequiredArgsConstructor;
import org.example.application.infrastructure.ui.ViewHelper;

@RequiredArgsConstructor
public class UserView {
    private final ViewHelper viewHelper;

    public User readUser() {
        String login = viewHelper.readString("Enter login: ");
        String password = viewHelper.readString("Enter password: ");
        return new User(login, password);
    }

    public void showError(String error){
        viewHelper.showError(error);
    }
}
