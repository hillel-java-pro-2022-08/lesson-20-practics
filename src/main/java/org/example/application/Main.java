package org.example.application;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.application.auth.AuthService;
import org.example.application.auth.UserStorage;
import org.example.application.auth.UserView;
import org.example.application.auth.menu.LoginMenuItem;
import org.example.application.auth.menu.LogoutMenuItem;
import org.example.application.auth.menu.RegisterMenuItem;
import org.example.application.auth.storage.InMemoryUserStorage;
import org.example.application.auth.storage.JsonFileUserStorage;
import org.example.application.auth.storage.TextFileUserStorage;
import org.example.application.infrastructure.menu.ExitMenuItem;
import org.example.application.infrastructure.menu.Menu;
import org.example.application.infrastructure.menu.MenuItem;
import org.example.application.infrastructure.ui.ViewHelper;

import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ViewHelper viewHelper = new ViewHelper(scanner);
        ObjectMapper objectMapper = new ObjectMapper();

        UserView userView = new UserView(viewHelper);
        //UserStorage userStorage = new InMemoryUserStorage();
        //UserStorage userStorage = new JsonFileUserStorage("./users.json",objectMapper);
        UserStorage userStorage = new TextFileUserStorage("./users.txt");
        AuthService authService = new AuthService(userStorage);

        List<MenuItem> items = List.of(
                new LoginMenuItem(authService, userView),
                new RegisterMenuItem(authService,userView),
                new LogoutMenuItem(authService),
                new ExitMenuItem()
        );

        Menu menu = new Menu(items, scanner);
        menu.run();
    }
}
